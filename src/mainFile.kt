fun main() {
   val person = Person("Ivan")
    println(person.sayHello())
    val person2 = Person("Alexey")
    val listPerson = listOf(person, person2)
    println(listPerson)
}

data class Person(val name: String) {
    fun sayHello() = "Hello, $name"
}